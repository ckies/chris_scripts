const _ = require('underscore');
const fs = require('fs');
const cheerio = require('cheerio');

const args = process.argv.slice(2);

fs.readFile(__dirname + '/' + args[0], function(err, data) {
	if (err) {
		throw err;
	}
	const $ = cheerio.load(data.toString());

	const ignore = [
		'com.sun.accessibility',
		'com.sun.swing',
		'java.awt',
		'com.sun.awt',
		'com.sun.demo',
		'com.sun.rowset',
		'sun.jdbc',
		'java.awt',
		'sun.font',
		'java.lang.instrument',
		'com.sun.script',
		'sun.org.mozilla.javascript',
		'java.lang.management',
		'javax.annotation',
		'javax.script',
		'javax.smartcardio',
		'javax.jnlp',
		'sun.net.ftp',
		'com.sun.org.glassfish',
		'sun.invoke',
		'java.lang.invoke',
		'com.sun.nio',
		'sun.print',
		'Exception'
	];

	const ignoreMethods = [ 'hashCode', 'equals' ];

	const toc = $('.toc').first();
	const toc_children = toc.children('table').first().children('tbody').first().children();
	for (let i = 0; i < toc_children.length; i++) {
		const toc_child = toc_children[i];
		const cols = $(toc_child).children();
		const indication = $(cols[0]).text();
		if (indication.includes('REMOVED') || indication.includes('UNCHANGED')) {
			ignore.push($(cols[1]).children('a').first().text());
		}
	}

	const ignorer = new RegExp(ignore.join('|'), 'g');
	const methodIgnorer = new RegExp(ignoreMethods.join('|'), 'g');

	console.log('||return||signature||source||target||');

	const klasses = $('.class');
	for (let i = 0; i < klasses.length; i++) {
		const klass = klasses[i];
		const className = $(klass).attr('id').trim();
		if (className.match(ignorer)) continue; // ignore

		const parsed_constructors = [];
		const parsed_methods = [];

		const constructors = $(klass).children('.class_constructors').first();
		if (constructors) {
			const tbody = constructors.children('table').first().children('tbody').first();
			const rows = tbody.children();

			if (rows.length !== 0) {
				for (let j = 0; j < rows.length; j++) {
					const td = $(rows[j]).children();

					const status = $(td[0]).text();
					if (status.includes('REMOVED') || status.includes('UNCHANGED')) continue; // dont care

					const c = $(td[2])
						.text()
						.replace(/.*\(/g, '(') // replace everything leading up to initial paren
						.replace('<wbr>', '') // remove whitespace break
						.replace(/[\s]+/g, ' ') // remove extraneous whitespace, replace with only 1
						.trim();
					if (c == '()') continue;

					const constructor = className + '.init' + c;
					parsed_constructors.push('|' + className + '|' + constructor.trim() + '| | |');
				}
			}
		}

		const methods = $(klass).children('.class_methods').first();
		if (methods) {
			const tbody = methods.children('table').first().children('tbody').first();
			const rows = tbody.children();
			if (rows.length !== 0) {
				for (let j = 0; j < rows.length; j++) {
					const td = $(rows[j]).children();

					const status = $(td[0]).text();
					if (status.includes('REMOVED') || status.includes('UNCHANGED')) continue; // dont care

					const type = $(td[2]).children('.method_return_type').first().text();

					const method = $(td[3])
						.text()
						.replace('<wbr>', '') // remove whitespace break
						.replace(/[\s]+/g, ' ') // remove extraneous whitespace, replace with only 1
						.trim();

					if (method.match(methodIgnorer)) continue; // ignore

					parsed_methods.push('|' + type.trim() + '|' + className + '.' + method + '| | |');
				}
			}
		}

		// print out results
		if (parsed_constructors !== 0 || parsed_methods !== 0) {
			if (parsed_constructors.length !== 0) {
				parsed_constructors.forEach((p) => console.log(p));
			}
			if (parsed_methods.length !== 0) {
				parsed_methods.forEach((p) => console.log(p));
			}
			console.log('');
		}
	}
});
