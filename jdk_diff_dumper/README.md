This script dump the output from https://github.com/AdoptOpenJDK/jdk-api-diff into Confluence Wiki compatible tables

Usage:
```bash
node index.js inputfile.html > output.txt
```