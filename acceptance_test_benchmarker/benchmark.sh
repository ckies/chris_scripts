#!/bin/bash

tests=(
    "AssessMemoryCanaryIT"
    "BeansIT"
    "BufferedReaderIT"
    "CSRFProtectIT"
    "CmdInjectionIT"
    "ELInjectionIT"
    "FileIT"
    "FreemarkerXSSIT"
    "GrizzlyIT"
    "HibernateIT"
    "HttpServlet25IT"
    "HttpServlet31IT"
    "InventoryIT"
    "JAXRS_2_0_RouteCoverageIT"
    "JAXRS_2_1_RouteCoverageIT"
    "JRegexIT"
    "JSR356WebSocketIT"
    "JavaMailIT"
    "JdbcIT"
    "JerseyIT"
    "JspAccessRuleIT"
    "JspIT"
    "LibraryFindingIT"
    "LogbackDeadzoneIT"
    "MulesoftIT"
    "ObjectInputStreamIT"
    "OverlyPermissiveCrossDomainPolicyIT"
    "ReflectedXSSIT"
    "RegexIT"
    "RichFacesCVEIT"
    "SSLSocketFactoryIT"
    "Spring3IT"
    "SpringDeadZoneIT"
    "SpringOrderByValidatorIT"
    "SpringRouteCoverageIT"
    "UnsafeFileUploadIT"
    "UrlIT"
    "WebGoatIT"
    "XXEInjectionIT"
)

echo "" > benchmark_results.txt

echo "formatting"
mvn fmt:format > /dev/null 2>&1
for i in "${tests[@]}"
do
    echo $i >> benchmark_results.txt
    echo "running $i"
    { time mvn clean install -pl contrast-agent-acceptance-tests -Dit.test=$i > /dev/null 2>&1 ; } 2>&1 | grep real >> benchmark_results.txt

done
